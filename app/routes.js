import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import DefaultLayout from './components/DefaultLayout';
import ResumeBox from './components/Dashboard/layout/ResumeBox.js';

import Login from './components/Login/Login.js';
import Dashboard from './components/Dashboard/Dashboard.js';

ReactDOM.render(
  <Router history={hashHistory}>

    <Route path="/" component={Login}>
    	<IndexRoute component={Login}></IndexRoute>
    	<Route path="/" component={Login}></Route>
    </Route>

    <Route path="dashboard" component={Dashboard}>
      	<IndexRoute component={Dashboard}></IndexRoute>
      	<Route path="dashboard" component={Dashboard}></Route>
    </Route>

  </Router>,
app);
