import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import './scss/style.scss';

class LoginN1 extends React.Component {

  componentDidMount() {
    let inner = document.getElementsByClassName('login-n1__inner')[0].offsetHeight;
    let height = screen.height;
    let margin = (height - inner) / 2 - 50;
  }

  render() {

    return (
      <div className="login-n1">
        <p><em>Seja bem vindo(a) <br/> ao primeiro e melhor administrador Vtex.</em></p>
        <div className="login-n1__inner">
          <h1 className="login-n1__title" ><span className="login-n1__title__name">DashN1</span> <em>Vtex Dashboard</em> </h1>
          <form action="" id="login">
            <div className="login-n1__field">
              <input id="email" className="login-n1__field__email" type="text" />
              <label htmlFor="email">email <em>*</em></label>
            </div>
            <div className="login-n1__field">
              <input id="senha" className="login-n1__field__pass" type="password" />
              <label htmlFor="senha">senha <em>*</em></label>
            </div>
            <div>
              <Link to="dashboard"><button className="login-n1__send">Login</button></Link>
            </div>
          </form>
        </div>
        <p><em>Copyright &copy;2016 <Link to="http://www.agencian1.com.br">Agência N1</Link></em></p>
      </div>
      );

  }
}

export default LoginN1;
