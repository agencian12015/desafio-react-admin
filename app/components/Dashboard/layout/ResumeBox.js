import React from "react";

class ResumeBox extends React.Component {
	componentDidMount() {
		var options = {
		  useEasing : true, 
		  useGrouping : true, 
		  separator : '.', 
		  decimal : ',', 
		  prefix : 'R$', 
		  suffix : '' 
		};
		var numAnim = new CountUp(this.props.boxID, 0, this.props.theValue , 2, 1, options);
		numAnim.start();
	}

	render() {
		return (
			<div className="inner-box medium-3 columns">
				<i className={ this.props.boxIcon }></i>

				<div className="valuation">
					<span>{ this.props.theTitle }</span>
					<h2 id={ this.props.boxID }></h2>
				</div>
			</div>			
		)
	}
}
export default ResumeBox;