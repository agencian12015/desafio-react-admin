import React from "react";

class CardLastTransactions extends React.Component {
	render() {
		return (
			<div className="inner-box last-transactions">
				<div className="resume-card__header">
					<h2 className="title">Últimas vendas</h2>
					<a href="#" className="btn"><i className="ton-li-upload-1"></i> Ver todas</a>
				</div>

				<table>
					<thead>
						<tr>
							<th>Cliente</th>
							<th>Nº do pedido</th>
							<th>Valor</th>
							<th>Status</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td><a href="#">Loirinho</a><small>Data da compra: 08/09/2016</small></td>
							<td>v556092csat-01</td>
							<td>R$32,00</td>
							<td><div className="tag">Enviado</div></td>
						</tr>

						<tr>
							<td><a href="#">Claudinho</a><small>Data da compra: 08/09/2016</small></td>
							<td>v556122csat-02</td>
							<td>R$32,00</td>
							<td><div className="tag waiting-payment">Aguardando pagamento</div></td>
						</tr>

						<tr>
							<td><a href="#">Huguinho</a><small>Data da compra: 08/09/2016</small></td>
							<td>v426092csat-03</td>
							<td>R$32,00</td>
							<td><div className="tag payment-expired">Pagamento recusado</div></td>
						</tr>

						<tr>
							<td><a href="#">Luizinho</a><small>Data da compra: 08/09/2016</small></td>
							<td>v656292csat-04</td>
							<td>R$32,00</td>
							<td><div className="tag canceled">Cancelado</div></td>
						</tr>
					</tbody>
				</table>
			</div>			
		)
	}
}
export default CardLastTransactions;