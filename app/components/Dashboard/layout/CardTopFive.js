import React from "react";

class CardTopFive extends React.Component {
	render() {
		return (
			<div className="inner-box last-transactions">
				<div className="resume-card__header">
					<h2 className="title">Marketplace</h2>
					<h3 className="subtitle">Melhores sellers do mês</h3>
					<a href="#" className="btn"><i className="ton-li-upload-1"></i> Ver todas</a>
				</div>

				<table>
					<thead>
						<tr>
							<th>Seller</th>
							<th>Total de pedidos</th>
							<th>Faturamento</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td><a href="#">Americanas</a></td>
							<td>23.123</td>
							<td>R$32.000,00</td>
						</tr>

						<tr>
							<td><a href="#">Submarino</a></td>
							<td>31.3124</td>
							<td>R$72.123,00</td>
						</tr>

						<tr>
							<td><a href="#">Nike</a></td>
							<td>351</td>
							<td>R$89.921,00</td>
						</tr>

						<tr>
							<td><a href="#">Pomps</a></td>
							<td>2.016</td>
							<td>R$666.000,00</td>
						</tr>

						<tr>
							<td><a href="#">Ladyhair</a></td>
							<td>666.000</td>
							<td>R$666.666,66</td>
						</tr>
					</tbody>
				</table>
			</div>			
		)
	}
}
export default CardTopFive;