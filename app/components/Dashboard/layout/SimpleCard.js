import React from "react";

 class SimpleCard extends React.Component {
	render() {
		return (
			<div className={"resume-card " +this.props.boxColumns }>
				<div className="resume-card__content">
					{this.props.children}
				</div>
			</div>
		);
	}
}

export default SimpleCard;