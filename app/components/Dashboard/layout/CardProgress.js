import React from "react";

class CardProgress extends React.Component {
	render() {
		return (
			<div className="inner-box last-transactions">
				<div className="resume-card__header">
					<h2 className="title">Estatísticas</h2>
					<a href="#" className="btn"><i className="ton-li-upload-1"></i> Ver todas</a>
				</div>

				<div className="resume-card__progress">
					<span className="progress__stats__title">Idade: 15 - 30</span>
					<span className="progress__stats__percent">63%</span>
					<div className="progress__bar__outer">
						<div className="progress__bar__inner" style={{width: '63%'}}></div>
					</div>
				</div>
				<div className="resume-card__progress">
					<span className="progress__stats__title">Idade: 30 - 45</span>
					<span className="progress__stats__percent">31%</span>
					<div className="progress__bar__outer">
						<div className="progress__bar__inner" style={{width: '31%'}}></div>
					</div>
				</div>
				<div className="resume-card__progress">
					<span className="progress__stats__title">Idade: 45 - 60</span>
					<span className="progress__stats__percent">6%</span>
					<div className="progress__bar__outer">
						<div className="progress__bar__inner" style={{width: '6%'}}></div>
					</div>
				</div>
				<div className="resume-card__progress">
					<span className="progress__stats__title">Valor compra: R$0 - R$100</span>
					<span className="progress__stats__percent">55%</span>
					<div className="progress__bar__outer">
						<div className="progress__bar__inner" style={{width: '55%'}}></div>
					</div>
				</div>
				<div className="resume-card__progress">
					<span className="progress__stats__title">Valor compra: R$100 - R$500</span>
					<span className="progress__stats__percent">52%</span>
					<div className="progress__bar__outer">
						<div className="progress__bar__inner" style={{width: '52%'}}></div>
					</div>
				</div>
				<div className="resume-card__progress">
					<span className="progress__stats__title">Valor compra: acima de R$500</span>
					<span className="progress__stats__percent">3%</span>
					<div className="progress__bar__outer">
						<div className="progress__bar__inner" style={{width: '3%'}}></div>
					</div>
				</div>
			</div>			
		)
	}
}
export default CardProgress;