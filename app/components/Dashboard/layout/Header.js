import React from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
	constructor() {
		super()
		this.state = {
			collapsed: true,
		};
	}

	toggleCollapse() {
		const collapsed = !this.state.collapsed;
		this.setState({collapsed});
	}

	render() {
		const { location } = this.props;
		const { collapsed } = this.state;
		const featuredClass = location.pathname === "/" ? "active" : "";
		const archivesClass = location.pathname.match(/^\/archives/) ? "active" : "";
		const settingsClass = location.pathname.match(/^\/settings/) ? "active" : "";
		const navClass = collapsed ? "collapse" : "";

		return (
			<header className="dashboard__header" role="navigation">
				<div className="dashboard__header__inner">
					<div className="container">
						<div className="dashboard__header__inner__iconmenu">
							<div className="iconmenu">
								<i className="square"></i><i className="square"></i><i className="square"></i>
								<i className="square"></i><i className="square"></i><i className="square"></i>
								<i className="square"></i><i className="square"></i><i className="square"></i>
							</div>
							<div className="dashboard__header__inner__iconmenu__menu-principal">
								<div className="container">
									<ul>
										<li></li>
									</ul>
								</div>
							</div>
						</div>
						<div className="dashboard__header__inner__logo">
							<h1>DASHN1</h1>
						</div>
						<div className="dashboard__header__inner__menu">
							<ul>
								<li><Link to="dashboard"><i className="ton-li-gong"></i><span className="count">3</span></Link></li>
								<li><Link to="dashboard"><i className="ton-li-refresh-1"></i></Link></li>
								<li className="dashboard__header__inner__menu__user">
									<div className="dashboard__header__inner__menu__user__info">
										<img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&f=y" />
										<div className="dashboard__header__inner__menu__user__info__text">
											<p className="dashboard__header__inner__menu__user__info__text__name">Olá <strong>Juninho</strong></p>
											<p className="dashboard__header__inner__menu__user__info__text__email">claudio.jr@agencian1.com.br</p>
										</div>
									</div>
									<ul>
										<li><Link to="dashboard">Configurações <i className="ton-li-gear-1"></i></Link></li>
										<li><Link to="dashboard">Sair <i className="ton-li-on-off"></i></Link></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</header>
		);
	}
}