import React from "react";
import { Link } from "react-router";
import "./scss/header.scss";
import "./scss/resumecard.scss";

import Header from "./layout/Header.js";
// Card Container
import SimpleCard from "./layout/SimpleCard.js";
// Cards
import ResumeBox from "./layout/ResumeBox.js";
import CardLastTransactions from "./layout/CardLastTransactions.js";
import CardTopFive from "./layout/CardTopFive.js";
import CardProgress from "./layout/CardProgress.js";

		
export default class extends React.Component {
	HttpClient = () => {
		
		this.get = function(aUrl, aCallback) {
	    var anHttpRequest = new XMLHttpRequest();
	    anHttpRequest.onreadystatechange = function() { 
	        if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
	            aCallback(anHttpRequest.responseText);
		    }

		    anHttpRequest.open( "GET", aUrl, true );            
		    anHttpRequest.setRequestHeader("Content-Type", "text/plain");
			anHttpRequest.setRequestHeader("Accept", "text/xml");
		    anHttpRequest.setRequestHeader("X-VTEX-API-AppToken", "AYEDYVHDMUFMUINOZAZTJGCBNARIUTDZJSOFMXDQEHGGCMHYGUVHXKKUWCPYEZJTRGDBTHBCWJPCAVZASSBZXWBYIUJKIFLEYMPVJGEZSTKOUHUADOFLYJVVFETZYRUR");
		    anHttpRequest.setRequestHeader("X-VTEX-API-AppKey", "vtexappkey-lealtexonline-BLIYSB");
		    
		    anHttpRequest.send( null );
		    // xmlhttp.setRequestHeader("X-VTEX-API-AppToken", "photoId");
		    // xmlhttp.setRequestHeader("X-VTEX-API-AppToken", "photoId");

		}

		// var aClient;
		// aClient = new HttpClient();
		this.get("http://lealtexonline.com.br/api/oms/pvt/orders/", function(response) {
			// do something with response
			console.log(response);
		});
		console.log("sucesso");
	}
	

  render() {
  	
    const { location } = this.props;
    return (
      <div className="dashboard">
        <Header location={location} />

        <div className="container">
			<SimpleCard boxColumns="medium-12 columns">
				<ResumeBox boxID="box1" boxIcon="ton-li-chart-3" theTitle="Aumento nas vendas" theValue="100000"  />
				<ResumeBox boxID="box2" boxIcon="ton-li-chart-5" theTitle="Total em Boleto" theValue="43100"  />
				<ResumeBox boxID="box3" boxIcon="ton-li-chart-4" theTitle="Total em Cartões" theValue="756000"  />
				<ResumeBox boxID="box4" boxIcon="ton-li-pie-chart-1" theTitle="Total do mês" theValue="423000"  />
			</SimpleCard>

			<div className="medium-12 large-8 columns padding-left-zero">
				<SimpleCard boxColumns="row columns">
					<CardLastTransactions />
				</SimpleCard>
			</div>

			<SimpleCard boxColumns="medium-4 columns">
				<CardTopFive />
			</SimpleCard>

			<div className="medium-12 large-12 columns no-padding">
				<SimpleCard boxColumns="medium-6 columns">
					<CardProgress />
				</SimpleCard>

				<SimpleCard boxColumns="medium-6 columns">
					<CardProgress />
				</SimpleCard>
			</div>

			<div className="medium-6 large-6 columns padding-left-zero">
				<SimpleCard boxColumns="medium-12 columns">
					<CardProgress />
				</SimpleCard>
			</div>

			<div className="medium-6 large-6 columns no-padding">
				<SimpleCard boxColumns="medium-12 columns">
					<CardProgress />
				</SimpleCard>
			</div>


		</div>
        {this.HttpClient()}

      </div>
    );
  }
}