import React from 'react';
import ReactDOM from 'react-dom';
import './scss/reset.scss';
import './scss/normalize.scss';
import './scss/base.scss';

class DefaultLayout extends React.Component {

  render() {


    return (
      <div>
        <h1> Nav | DefaultLayout </h1>

        <div class="content">
        {this.props.children}
        </div>

        <h1> Footer </h1>
      </div>
      );

  }
}

export default DefaultLayout;
